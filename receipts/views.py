from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, CreateView
from receipts.models import Receipt, ExpenseCategory, Account

# Create your views here.


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/receipts_list.html"
    context_object_name = "receipt_list"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "categories/expense_category_list.html"
    context_object_name = "expense_category"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/accounts_list.html"
    context_object_name = "account_list"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    fields = ["vendor", "total", "tax", "date", "account"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.save()
        return redirect("receipts/receipt_create.html")


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    fields = ["name", "owner"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.save()
        return redirect("list_expense")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    fields = ["name", "number", "owner"]

    def form_valid(self, form):
        item = form.save(commit=False)
        item.save()
        return redirect("list_account")
