from django.urls import include, path, reverse_lazy
from receipts.views import (
    ReceiptListView,
    AccountCreateView,
    ExpenseCategoryCreateView,
    ReceiptCreateView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path(
        "accounts/create", AccountCreateView.as_view(), name="create_account"
    ),
    path(
        "categories/create",
        ExpenseCategoryCreateView.as_view(),
        name="create_expense_category",
    ),
    path("create", ReceiptCreateView.as_view(), name="create_receipt"),
]
