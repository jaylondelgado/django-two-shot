from django.db import models
from django.conf import settings
from django.shortcuts import redirect

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.


class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name


class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3)
    date = models.DateTimeField(auto_now_add=True)
    purchaser = models.ForeignKey(
        USER_MODEL, related_name="receipts", on_delete=models.CASCADE
    )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        Account,
        related_name="receipts",
        null=True,
        on_delete=models.DO_NOTHING,
    )

    def form_valid(self, form):
        item = form.save(commit=False)
        item.user_property = self.request.user
        item.save()
        return redirect("home")
